function exampleFunction() {
    // Intentional use of an undeclared variable
    undeclaredVariable = "Hello, World!";

    // Intentional unused variable
    var unusedVariable = 42;

    // Intentional division by zero to create an issue
    var result = divideByZero();

    // Logging the result
    console.log("Result:", result);
}

function divideByZero() {
    var numerator = 10;
    var denominator = 0; // Intentional division by zero

    // This line will trigger a runtime exception
    return numerator / denominator;
}

// Calling the function to trigger the issues
exampleFunction();

